<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Municipalidad de Illapel</title>

      <link rel="stylesheet" href="assets/css/login.css">
      <link rel="stylesheet" href="assets/css/font-awesome.min.css">
      <script src="assets/js/jquery-3.2.1.js"></script>
      <script src="assets/js/login.js"></script>
      
    </head>
    
    <body>

        <img id="fondo" src="assets/images/fondo.png" alt="background image" />

        <form action="controlador/1 principal.php?usuario=<?php echo"inicio";?>" method="post" class="form_contact">
            <h1>Test de seguridad empresarial</h1>
            <div class="inset">
            <p>
                <label for="email">Ingrese su usuario</label>
                <input type="text" name="user" id="user" placeholder="Ejemplo: Loudred123" autocomplete="off" required>
            </p>
             <p>
                <label for="password">Ingrese contraseña</label>
                <input type="password" name="password" id="password" placeholder="********" autocomplete="off" required>
            </p>

            <button type="reset" value="Reset">Limpiar</button>

            </div>

            <span style="cursor:pointer" id="recordar">¿Olvidaste tu contraseña?</span>

            <p class="p-container">
                <a href="controlador/0 registro.php"><input type='button' name='up' id='up' value='Registrarse'></a>
                <input type="submit" name="go" id="go" value="Continuar">
            </p>
        </form>

        <div class="footer">
            <p></p>
        </div>

      </body>

</html>